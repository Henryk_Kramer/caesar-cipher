//gets all input fields
let inputLeft = document.getElementById("inputLeft");
let outputLeft = document.getElementById("outputLeft");
let inputRight = document.getElementById("inputRight");
let outputRight = document.getElementById("outputRight");
let keyInputLeft = document.getElementById("keyInputLeft");
let keyInputRight = document.getElementById("keyInputRight");

//the de- and encrypting alphabet
let alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l',
'm','n','o','p','q','r','s','t','u','v','w','x','y','z'];

function encrypt() {
  //setup
  let x = 0;
  let key = 0;
  let input = inputLeft.value.toLowerCase().split('');
  outputLeft.value = "";
  keyInputLeft.className = "";

  //iterate over every input character
  for(variable of input) {

    //preparation
    x = alphabet.indexOf(variable);
    key = alphabet.indexOf(keyInputLeft.value.toLowerCase());

    //if key does not exist in alphabet the text will not be no encrypted
    if(key < 0) {
        keyInputLeft.className = "wrongInput";
        outputLeft.value = input.join("");
        return;
    }

    //pick the right letter
    if(x < 0) {
      outputLeft.value += variable;
    } else if(x+key > 25) {
      outputLeft.value += alphabet[x+key-26];
    } else {
      outputLeft.value += alphabet[x+key];
    }

  }
}

function decrypt() {
  //setup
  let x = 0;
  let key = 0;
  let input = [];
  input = inputRight.value.toLowerCase().split('');
  outputRight.value = "";
  keyInputRight.className = "";

  //iterate over every input character
  for(variable of input) {

    //preparation
    x = alphabet.indexOf(variable);
    key = alphabet.indexOf(keyInputRight.value.toLowerCase());

    //if key does not exist in alphabet the text will not be no decrypted
    if(key < 0) {
        keyInputRight.className = "wrongInput";
        outputLeft.value = input.join("");
        return;
    }

    //pick the right letter
    if(x < 0) {
      outputRight.value += variable;
    } else if(x-key < 0) {
      outputRight.value += alphabet[x-key+26];
    } else {
      outputRight.value += alphabet[x-key];
    }

  }
}
