Author: Henryk Kramer
Created: Spring 2018
Refactored: February 2019

This program lets you en- and decrypt your message with the Caesar Cipher. It is called that, for Caesar is said to have used this method. To en- and decrypt your message, each character is shifted by the given key in a predefined alphabet (in this case: a-z) and rotates back to the beginning if 'key + character' is greater than the last position in the alphabet.

Title: opens wikipedia-link for further information

left side:
- key(a-z): shifts the plaintext by key (when key = n, than a = n)
- plaintext: text to encrypt
- ciphertext: encrypted text

right side:
- key(a-z): shifts the plaintext by key (when key = n, than n = a)
- ciphertext: text to decrypt
- plaintext: decrypted text

